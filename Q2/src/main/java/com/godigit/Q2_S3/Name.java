package com.godigit.Q2_S3;

public class Name {
	private String firstName;
	private String middleName;
	private String lastName;
	@Override
	public String toString() {
		return "FullName [ " + firstName + " " + middleName + " " + lastName + "]";
		}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	


}
