/*Field or Property-based Dependency Injection

 * here, the dependencies of the class 'name' are injected expicitly by beans.xml file by setter method.
 * setters for the members of class 'name' is created.
 * a class called 'fullname' is created which has a non primitive object of class 'name'.
 * the dependencies for class 'name' is given via 'fullname ' through "ref" attribute.
 * values for each member of 'name' is  given using 'property' tag inside a bean in xml file
 * then the bean is called using its id from beans.xml file.
 * note: no constructors are created here!
 * */

package com.godigit.Q2_S3;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class TestFullName {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
		FullName fName = (FullName)context.getBean("Question3");
		System.out.println(fName.getFullname());

	}

}
