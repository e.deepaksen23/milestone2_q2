/*Setter based Dependency Injection


 * here, the dependencies of the class 'vaccinestatus' are injected expicitly by beans.xml file by setter method.
 * setters for the members of class 'vaccinestatus' is created.
 * values for each member of 'vaccinestatus' is  given using 'property' tag inside a bean in xml file
 * then the bean is called using its id from beans.xml file.
 * note: no constuctors are created here!
 * */


package com.godigit.Q2_S2;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;



public class VaccineStatusCheck {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
		VaccineStatus details = (VaccineStatus)context.getBean("Question2");
		System.out.println("NAME : "+details.getName());
		System.out.println("AGE : "+details.getAge());
		System.out.println("VACCINATED : "+details.isStatus());
			}

}
