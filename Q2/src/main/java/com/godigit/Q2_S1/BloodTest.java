/*Constructor based Dependency Injection

 * here, the dependencies of the class 'blood' are injected expicitly by beans.xml file by constructor method.
 * constructor for the class 'blood' is created.
 * values for each member of blood is  given using 'constructor-arg' tag inside a bean in xml file
 * then the bean is called using its id from beans.xml file.
 * note: no setters are created here!
 * */


package com.godigit.Q2_S1;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class BloodTest {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
		Blood blood = (Blood)context.getBean("Question1");
		System.out.println("BLOOD GROUP: "+blood.getBloodGroup());
		System.out.println("RBC COUNT: "+blood.getRBC_count());
		System.out.println("WBC COUNT : "+blood.getWBC_count());
		System.out.println("PLATELET COUNT : "+blood.getPlatelet_count());
	}

}
