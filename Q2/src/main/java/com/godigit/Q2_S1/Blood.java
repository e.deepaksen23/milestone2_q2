package com.godigit.Q2_S1;

public class Blood {
	private String bloodGroup;
	private int RBC_count;
	private int WBC_count;
	private int platelet_count;
	public Blood(String bloodGroup, int rBC_count, int wBC_count, int platelet_count) {
		super();
		this.bloodGroup = bloodGroup;
		RBC_count = rBC_count;
		WBC_count = wBC_count;
		this.platelet_count = platelet_count;
	}
	public Blood() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getBloodGroup() {
		return bloodGroup;
	}
	public int getRBC_count() {
		return RBC_count;
	}
	public int getWBC_count() {
		return WBC_count;
	}
	public int getPlatelet_count() {
		return platelet_count;
	}
	
	

}
